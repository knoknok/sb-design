Display
GND => GND
VCC => 3V3
D0 => A3
D1 => A5
RST => D5
Dc => D3
CS => D4

Proximity
VCC => VIN
TRIG => D1
ECHO => D6
GND => GND

Button
GND => GND
TRIG => D2